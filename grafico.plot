set datafile separator ";"

stats 'datos.csv' using 1:2
MAX_leliq_saldo = STATS_max_y

stats 'datos.csv' using 1:3
MAX_reservas = STATS_max_y

stats 'datos.csv' using 1:4
MAX_dolar = STATS_max_y

stats 'datos.csv' using 1:5
MAX_leliq_interes = STATS_max_y

stats 'datos.csv' using 1:6
MAX_inflacion_interanual = STATS_max_y

stats 'datos.csv' using 1:7
MAX_inflacion_acumulada = STATS_max_y

stats 'datos.csv' using 1:8
MAX_riesgo_pais = STATS_max_y

stats 'datos.csv' using 1:9
MAX_dolar_ccl = STATS_max_y

stats 'datos.csv' using 1:10
MAX_circulante = STATS_max_y

stats 'datos.csv' using 1:11
MAX_inflacion_acumulada_macri = STATS_max_y

set xdata time
set timefmt "%d/%m/%Y"
set format x "%m/%Y"
set xrange ["11/01/2018":]
set xtics "11/01/2018",2592000

rango = 280

set yrange [0:(rango*10000)]
set ytics 0, 100000 nomirror
set format y "%.0f"

set y2range [0:(rango)]
set y2tics 0, 10 nomirror

set grid
set key top left 
set key Left reverse
set key width -12

set decimalsign ','

set terminal pngcairo font "sans,12" size 1920,1080
archivo_salida = sprintf ( "grafico-bcra-%d-rango.png", rango)
set output archivo_salida

lineas = ''
separador = ''

if ( MAX_leliq_saldo < (rango*10000) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:2 axes x1y1 linewidth 2 linetype 1 with lines title columnheader")
 separador = ', '
}

if ( (MAX_reservas/1000) < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:($3/1000) axes x1y2 linewidth 2 linetype 2 with lines title columnheader")
 separador = ', '
}

if ( MAX_dolar < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:4 axes x1y2 linewidth 2 linetype 3 with lines title columnheader")
 separador = ', '
}

if ( MAX_leliq_interes < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:5 axes x1y2 linewidth 2 linetype 4 with lines title columnheader")
 separador = ', '
}

if ( ((MAX_reservas/MAX_dolar)/1000) < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:(($2/$4)/1000) axis x1y2 linewidth 2 linetype 5 with lines title 'Saldo LeLiq en Dólares (eje derecho, en miles de millones)'")
 separador = ', '
}

if ( MAX_inflacion_interanual < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:6 axes x1y2 linewidth 2 linetype 6 with lines title columnheader")
 separador = ', '
}

if ( MAX_inflacion_acumulada < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:7 axes x1y2 linewidth 2 linetype rgb '#930000' with lines title columnheader")
 separador = ', '
}

if ( MAX_riesgo_pais < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:8 axes x1y2 linewidth 2 linetype 8 with lines title columnheader")
 separador = ', '
}

if ( MAX_dolar_ccl < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:9 axes x1y2 linewidth 2 linetype rgb '#00FF88' with lines title columnheader")
 separador = ', '
}

if ( MAX_circulante < (rango*10000) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:10 axes x1y1 linewidth 2 linetype rgb '#FF008D' with lines title columnheader")
 separador = ', '
}

if ( MAX_inflacion_acumulada_macri < (rango) ) {
 lineas = sprintf("%s%s%s", lineas, separador, "'datos.csv' using 1:11 axes x1y2 linewidth 2 linetype 7 with lines title columnheader")
 separador = ', '
}

eval ('plot '.lineas)
